# Django Stub

Шаблон проекта со автоматическим развёртыванием через Ansible.

## Создание проекта

Скопируйте репозиторий к себе и запустите `./startproject.sh` в корне проекта.

Или запустите следующий однострочник:

```
echo -n "Project directory: " && read PROJECT_DIR &&\
git clone git@bitbucket.org:redisca/django-stub-git.git $PROJECT_DIR &&\
cd $PROJECT_DIR && rm -rf .git && ./startproject.sh && cd -
```

## Развертывание

### Требования к окружению

**На локалхосте:** Ansible 2.7+, Rsync.

**На сервере:** Ubuntu 18.04 LTS

### Шаг 1

Откройте `ansible/stage.ini` и укажите в секции `appservers` IP адрес сервера:

```
[appservers]
192.168.0.1
```

### Шаг 2

Запустите `./ansible/play.sh deploy stage`. Это поставит нужные системные зависимости, настроит nginx, postgres, создаст базу данных и запустит проект.
