#!/bin/bash

set -e
cd $(dirname "${BASH_SOURCE[0]}")
ANSIBLE_DIR=ansible

OLD_NAME="project"
if [ ! -d $OLD_NAME ]; then
    echo "Project is already initialized"
    exit 0
fi

NEW_NAME=$1
if [[ $NEW_NAME == "" ]]; then
    while true; do
        echo -n "Enter project name: "
        read NEW_NAME
        if [[ $NEW_NAME =~ ^[a-z0-9_]+$ ]]; then
            break
        fi
        echo "Project name can only contain symbols [a-z0-9_]"
        echo "because it's a fucking python module!"
    done
fi

case "$OSTYPE" in
    darwin*)  PLATFORM="OSX" ;;
    linux*)   PLATFORM="LINUX" ;;
    bsd*)     PLATFORM="BSD" ;;
    *)        PLATFORM="UNKNOWN" ;;
esac

if [[ "$PLATFORM" == "UNKNOWN" ]]; then
    echo "Unknown platform"
    exit 0
fi

replace() {
    if [[ "$PLATFORM" == "OSX" || "$PLATFORM" == "BSD" ]]; then
        sed -Ei "" "$1" "$2"
    elif [ "$PLATFORM" == "LINUX" ]; then
        sed -Ei "$1" "$2"
    fi
}

generate_key() {
    length=$1
    echo $(python -c "import random, string; print(''.join(random.choice(string.ascii_letters + string.digits) for _ in range($length)))")
}

# Change project name in python files
replace "s/${OLD_NAME}/${NEW_NAME}/g" manage.py
replace "s/${OLD_NAME}/${NEW_NAME}/g" $OLD_NAME/wsgi.py
replace "s/([^a-z0-9])${OLD_NAME}([^a-z0-9])/\1${NEW_NAME}\2/g" $OLD_NAME/settings.py

# Change project name in Ansible configs
replace "s/${OLD_NAME}/${NEW_NAME}/g" "$ANSIBLE_DIR/stage.ini"
replace "s/${OLD_NAME}/${NEW_NAME}/g" "$ANSIBLE_DIR/env_vars/base.yml"
replace "s/${OLD_NAME}/${NEW_NAME}/g" "$ANSIBLE_DIR/env_vars/stage.yml"

# Generate app secret key
_secret_key=$(generate_key 64)
replace "s/__secret_key__/${_secret_key}/g" "$ANSIBLE_DIR/env_vars/stage.yml"
unset _secret_key

# Generate postgres password
_pg_password=$(generate_key 16)
replace "s/__pg_password__/$_pg_password/g" "$ANSIBLE_DIR/env_vars/stage.yml"
unset _pg_password

# Rename project folder
mv $OLD_NAME $NEW_NAME

# Generate readme
mv README.md.template README.md
replace "s/__project__/$NEW_NAME/g" README.md

# Remove this script
rm -- "$0"
