from django.urls import path
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings

from .views import health_check

urlpatterns = [
    path('django/', admin.site.urls),
    path('', health_check),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
